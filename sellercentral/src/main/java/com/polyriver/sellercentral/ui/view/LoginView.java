package com.polyriver.sellercentral.ui.view;

import com.polyriver.app.model.Merchant;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.security.SecurityService;
import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = "login")
public class LoginView extends VerticalLayout implements View, HasLogger {

	private static final long serialVersionUID = 5085234484000922192L;

	public LoginView(SecurityService securityService) {
		FormLayout loginForm = initComponents(securityService);
		addComponent(loginForm);
		setSizeFull();
		setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
	}

	private FormLayout initComponents(SecurityService securityService) {
		FormLayout loginForm = new FormLayout();
		loginForm.setSizeUndefined();

		TextField username = new TextField("Username");
		loginForm.addComponent(username);

		PasswordField password = new PasswordField("Password");
		loginForm.addComponent(password);

		Button forgot = new Button("Forgot Password", event -> {
			getUI().getNavigator().navigateTo("forgot");
		});
		forgot.setStyleName("link");
		loginForm.addComponent(forgot);

		// Register link
		Button register = new Button("Sign Up", event -> {
			getUI().getNavigator().navigateTo("register");
		});
		register.setStyleName("link");
		loginForm.addComponent(register);

		Button loginButton = new Button("Login");
		loginButton.setEnabled(false);

		loginButton.addClickListener(event -> {
			boolean loginSucess = securityService.login(username.getValue(), password.getValue());
			if (!loginSucess) {
				password.setValue("");
				Notification.show("Login failed", Notification.Type.ERROR_MESSAGE);
				username.focus();
			} else {
				VaadinService.getCurrentRequest().setAttribute("username", username.getValue());
				getUI().getNavigator().navigateTo("dashboard");
			}
		});

		attachBinder(username, password, loginButton);

		loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		loginForm.addComponent(loginButton);

		return loginForm;

	}

	private void attachBinder(TextField username, PasswordField password, Button loginButton) {
		Binder<Merchant> binder = new Binder<>();

		binder.forField(username).asRequired("User name is empty.").bind(Merchant::getUsername, Merchant::setUsername);

		binder.forField(password).asRequired("Password is empty").bind(Merchant::getPassword, Merchant::setPassword);

		binder.setBean(new Merchant());

		binder.addStatusChangeListener(event -> loginButton.setEnabled(binder.isValid()));
	}

}
