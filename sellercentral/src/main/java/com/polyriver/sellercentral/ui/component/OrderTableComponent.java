package com.polyriver.sellercentral.ui.component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;

import com.polyriver.amazon.model.Order;
import com.polyriver.amazon.service.OrderDataService;
import com.polyriver.sellercentral.HasLogger;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.DesignContext;

@SpringComponent
@UIScope
public class OrderTableComponent extends CustomComponent implements HasLogger {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9171410262123773635L;

	Grid<Order> orderGrid;

	private String title;

	public OrderTableComponent() {
		super();
	}

	@Autowired
	public OrderTableComponent(OrderDataService orderDataService) {

		getLogger().debug("OrderTableComponent");

		orderGrid = new Grid<>(Order.class);

		// A layout structure used for composition
		Panel panel = new Panel(getTitle());
		// The root of the component hierarchy
		VerticalLayout content = new VerticalLayout();

		panel.setContent(content);

		// Add some component
		content.addComponent(new Label("<b>Hello!</b> - How are you?", ContentMode.HTML));

		// orderGrid.setCaption("Amazing Grid");
		orderGrid.setSizeFull();

		List<Order> orders = new ArrayList<Order>();

		Iterable<Order> iterable = orderDataService.findAll();

		getLogger().debug("Found " + Stream.of(iterable).count() + " orders.");

		iterable.forEach(e -> orders.add(e));

		orderGrid.setItems(orders);
		orderGrid.setColumns("amazonOrderId", "orderItem");

		content.addComponent(orderGrid);

		setCompositionRoot(panel);
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void readDesign(Element design, DesignContext designContext) {
		super.readDesign(design, designContext);

		for (Attribute at : design.attributes()) {
			String s = at.getValue();
			getLogger().debug("Attribute value: " + s);
		}
	}

	public Grid<Order> getOrderGrid() {
		return orderGrid;
	}

	public void setOrderGrid(Grid<Order> orderGrid) {
		this.orderGrid = orderGrid;
	}

}
