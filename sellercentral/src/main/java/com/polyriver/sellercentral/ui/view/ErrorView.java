package com.polyriver.sellercentral.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Unused.
 * 
 * @author zarkasih.adlan
 *
 */
@SpringView(name = "error")
public class ErrorView extends CustomComponent implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3872185059096115016L;

	public ErrorView() {
		VerticalLayout layout = new VerticalLayout();

		layout.addComponent(new Label("Error!"));

		setCompositionRoot(layout);
	}

}
