package com.polyriver.sellercentral.ui.view;

import com.polyriver.app.model.Merchant;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.security.SecurityService;
import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = "reset")
public class ResetPasswordView extends VerticalLayout implements View, HasLogger {

	private static final long serialVersionUID = 7817532519847676381L;

	private long userId;

	public ResetPasswordView(SecurityService securityService) {
		FormLayout resetLayout = initComponents(securityService);
		addComponent(resetLayout);
		setSizeFull();
		setComponentAlignment(resetLayout, Alignment.MIDDLE_CENTER);
	}

	private FormLayout initComponents(SecurityService securityService) {
		FormLayout resetLayout = new FormLayout();
		resetLayout.setSizeUndefined();

		PasswordField newPassword = new PasswordField("New Password");
		PasswordField confirmPassword = new PasswordField("Confirm Password");

		Binder<Merchant> binder = new Binder<>();
		binder.forField(newPassword).asRequired().bind(Merchant::getPassword, Merchant::setPassword);
		binder.forField(confirmPassword).asRequired().bind(Merchant::getPassword, Merchant::setPassword);

		resetLayout.addComponent(newPassword);
		resetLayout.addComponent(confirmPassword);

		Button submit = new Button("Submit", evt -> {

			boolean passwordIsSame = newPassword.getValue().equals(confirmPassword.getValue());
			if (!passwordIsSame) {
				Notification.show("New password and confirm password must be same", Notification.Type.ERROR_MESSAGE);
				newPassword.setValue("");
				confirmPassword.setValue("");
				newPassword.focus();
			} else {
				boolean resetSucess = securityService.resetPassword(getUserId(), newPassword.getValue());
				if (!resetSucess) {
					Notification.show("Password reset failed.", Notification.Type.ERROR_MESSAGE);
					newPassword.setValue("");
					confirmPassword.setValue("");
					newPassword.focus();
				} else {
					final Window dialog = new Window("Password has been reset. Please login.");
					dialog.setModal(true);
					dialog.setResizable(false);
					dialog.setDraggable(false);
					dialog.setWindowMode(WindowMode.NORMAL);

					getUI().addWindow(dialog);
					getUI().getNavigator().navigateTo("login");
				}
			}
		});

		submit.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		resetLayout.addComponent(submit);

		return resetLayout;
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		getLogger().debug("Event parameters: " + event.getParameters());
		if (event.getParameters() != null) {
			setUserId(Long.valueOf(event.getParameters()));
		}
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}
