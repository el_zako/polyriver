package com.polyriver.sellercentral.ui.view;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.polyriver.app.model.Merchant;
import com.polyriver.app.repository.MerchantRepository;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.security.SecurityService;
import com.polyriver.sellercentral.service.EmailService;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = "forgot")
public class ForgotPasswordView extends VerticalLayout implements View, HasLogger {

	private static final long serialVersionUID = 7717532519847676381L;

	private static final String appURL = "http://localhost:8080";

	@Autowired
	EmailService emailService;

	public ForgotPasswordView(SecurityService securityService, MerchantRepository merchantRepository) {
		FormLayout forgotLayout = initComponents(securityService, merchantRepository);
		addComponent(forgotLayout);
		setSizeFull();
		setComponentAlignment(forgotLayout, Alignment.MIDDLE_CENTER);
	}

	private FormLayout initComponents(SecurityService securityService, MerchantRepository merchantRepository) {
		FormLayout forgotLayout = new FormLayout();

		TextField username = new TextField("Enter your e-mail address");

		forgotLayout.addComponent(username);

		Binder<Merchant> binder = attachBinder(username);

		binder.forField(username).asRequired("E-mail address may not be empty")
				.withValidator(new EmailValidator("E-mail address is not valid."))
				.bind(Merchant::getUsername, Merchant::setUsername);

		Button resetButton = new Button("Reset");
		resetButton.setEnabled(false);

		resetButton.addClickListener(evt -> {
			resetPassword(securityService, merchantRepository, username.getValue(), binder);
		});

		binder.addStatusChangeListener(event -> resetButton.setEnabled(binder.isValid()));

		resetButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		forgotLayout.addComponent(resetButton);

		forgotLayout.setWidth(null);

		return forgotLayout;
	}

	private Binder<Merchant> attachBinder(TextField username) {
		Binder<Merchant> binder = new Binder<>();

		binder.forField(username).asRequired("E-mail address may not be empty")
				.withValidator(new EmailValidator("E-mail address is not valid."))
				.bind(Merchant::getUsername, Merchant::setUsername);

		return binder;
	}

	private void resetPassword(SecurityService securityService, MerchantRepository merchantRepository, String username,
			Binder<Merchant> binder) {
		Merchant user = merchantRepository.findByUsername(username);

		if (user == null) {
			binder.setBean(user);
			// even if user not found, we show this message
			// to prevent attackers from guessing valid users
			Notification.show("Password has been reset. Please check your inbox.", Notification.Type.TRAY_NOTIFICATION);
			getLogger().warn("User name " + username + " not found.");

		} else {

			String token = UUID.randomUUID().toString();
			securityService.createPasswordResetTokenForUser(user, token);

			String message = "<html><body>" + "<h1>Change Your Password</h1>"
					+ "<p>We have received a password change request for your account (" + user.getUsername()
					+ "). </p>" + "<a href='" + appURL + "/reset?token=" + token
					+ "'>Click here to reset your password.</a>" + "</body></html>";

			if (emailService.sendSimpleMessage(user.getUsername(), "Reset Password Request", message)) {
				Notification.show("Password has been reset. Please check your inbox.",
						Notification.Type.TRAY_NOTIFICATION);

				getLogger().debug("Password reset link: " + "http://localhost:8080/reset?token=" + token);

				getUI().getNavigator().navigateTo("login");
			} else {
				Notification.show("Request failed. Try again later.");
			}
		}

	}

}
