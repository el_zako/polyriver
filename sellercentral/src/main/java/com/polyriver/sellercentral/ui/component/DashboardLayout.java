package com.polyriver.sellercentral.ui.component;

import java.io.InputStream;

import com.vaadin.annotations.DesignRoot;
//import com.vaadin.board.Board;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

/**
 * For new users with no custom design yet, we load the design from the same package.
 * 
 * For onboarded users, we load the design from the user layout table
 * 
 * @author zarkasih.adlan
 *
 */
@DesignRoot
public class DashboardLayout extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8811249943779087600L;

	//protected Board board;

	public DashboardLayout(InputStream is) {
		if (is != null) {
			Design.read(is, this);
		} else {
			// read the default design from classpath for new user
			Design.read(this);
		}

	}

}
