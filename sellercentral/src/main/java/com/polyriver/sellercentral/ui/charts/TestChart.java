package com.polyriver.sellercentral.ui.charts;

import com.polyriver.sellercentral.data.ChartsData;
import com.polyriver.sellercentral.data.ChartsData.ShoeSizeInfo;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;

public class TestChart extends Chart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4870717352175174527L;

	public TestChart() {
		initChart();
	}

	private void initChart() {
		// Chart chart = new Chart();
		Configuration conf = this.getConfiguration();
		conf.setTitle("Hello Charts!");
		conf.getChart().setType(ChartType.LINE);

		conf.getxAxis().setTitle("Shoe size (EU)");
		conf.getyAxis().setTitle("Age (Years)");

		ChartsData data = new ChartsData();

		DataSeries girls = new DataSeries("Girls");
		for (ShoeSizeInfo shoeSizeInfo : data.getGirlsData()) {
			// Shoe size on the X-axis, age on the Y-axis
			girls.add(new DataSeriesItem(shoeSizeInfo.getSize(), shoeSizeInfo.getAgeMonths() / 12.0f));
		}
		conf.addSeries(girls);

		DataSeries boys = new DataSeries("Boys");
		for (ShoeSizeInfo shoeSizeInfo : data.getBoysData()) {
			// Shoe size on the X-axis, age on the Y-axis
			boys.add(new DataSeriesItem(shoeSizeInfo.getSize(), shoeSizeInfo.getAgeMonths() / 12.0f));
		}
		conf.addSeries(boys);

	}

}
