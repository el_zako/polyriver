package com.polyriver.sellercentral.ui;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.polyriver.app.model.PasswordResetToken;
import com.polyriver.app.repository.PasswordResetTokenRepository;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.security.SecurityUtils;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI
@PushStateNavigation
@Theme(ValoTheme.THEME_NAME)
public class SecuredUI extends UI implements HasLogger {

	private static final long serialVersionUID = 6827465026608708766L;

	@Autowired
	private SpringNavigator navigator;

	@Autowired
	private PasswordResetTokenRepository passwordTokenRepository;

	@Override
	protected void init(VaadinRequest request) {
		getPage().setTitle("Polyriver Seller Central");

		navigator.init(this, this);

		if (SecurityUtils.isLoggedIn()) {
			String auth = SecurityContextHolder.getContext().getAuthentication().getName();
			getLogger().debug("User: " + auth);

			navigator.navigateTo("dashboard");
		} else {

			String path = request.getPathInfo();

			/**
			 * if path is reset, we check if the token exists and that it is valid, then
			 * navigate user to password reset page.
			 */
			if (path.equalsIgnoreCase("/reset")) {
				String token = request.getParameter("token");
				getLogger().debug("Token " + token);

				PasswordResetToken prt = passwordTokenRepository.findByToken(token);

				if (prt != null && isNotExpired(prt)) {
					getLogger().debug("Found valid token: " + prt.toString());
					// pass id as request parameter - we do not show
					// user name of password being reset for security purposes
					navigator.navigateTo("reset/" + prt.getMerchant().getId());
				} else {
					// TODO navigate back to forgot password view with message that token does not
					// exists or is expired
					navigator.navigateTo("error");
				}

			} else {
				navigator.navigateTo("login");
			}
		}

	}

	private boolean isNotExpired(PasswordResetToken token) {

		int i = LocalDateTime.now().compareTo(token.getExpiryDate());

		getLogger().debug("Current time compared to expiry date of reset token: " + i);

		if (i < 0) {
			getLogger().debug("Password reset token is still valid.");
			return true;
		} else {
			getLogger().debug("Password reset token is expired.");
			return false;
		}

	}

}
