package com.polyriver.sellercentral.ui.view;

import java.util.HashSet;
import java.util.Set;

import com.polyriver.app.model.Authorities;
import com.polyriver.app.model.Merchant;
import com.polyriver.app.repository.AuthorityRepository;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.security.SecurityService;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView
public class RegisterView extends VerticalLayout implements View, HasLogger {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7687833061631535177L;

	public RegisterView(SecurityService securityService, AuthorityRepository authorityRepository) {
		FormLayout form = initComponents(securityService, authorityRepository);
		addComponent(form);
		setSizeFull();
		setComponentAlignment(form, Alignment.MIDDLE_CENTER);
	}

	private FormLayout initComponents(SecurityService securityService, AuthorityRepository authorityRepository) {
		FormLayout form = new FormLayout();

		TextField customerName = new TextField("Your Name");
		TextField email = new TextField("Email");
		PasswordField password = new PasswordField("Password");

		form.setSizeUndefined();
		form.addComponent(customerName);
		form.addComponent(email);
		form.addComponent(password);

		Binder<Merchant> binder = attachBinder(customerName, email, password);

		Button registerButton = new Button("Register");
		registerButton.setEnabled(false);

		registerButton
				.addClickListener(event -> registerNewAccount(securityService, authorityRepository, binder.getBean()));
		binder.addStatusChangeListener(event -> registerButton.setEnabled(binder.isValid()));
		registerButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

		form.addComponent(registerButton);

		return form;
	}

	private Binder<Merchant> attachBinder(TextField customerName, TextField email, PasswordField password) {

		Binder<Merchant> binder = new Binder<>();

		binder.forField(customerName).asRequired("Name is empty").bind(Merchant::getCustomerName, Merchant::setCustomerName);

		binder.forField(email).asRequired("E-mail address may not be empty")
				.withValidator(new EmailValidator("E-mail address is not valid."))
				.bind(Merchant::getUsername, Merchant::setUsername);

		binder.forField(password).asRequired("Password may not be empty")
				.withValidator(new StringLengthValidator("Password must be at least 8 characters long", 8, null))
				.bind(Merchant::getPassword, Merchant::setPassword);

		Label validationStatus = new Label();
		binder.setStatusLabel(validationStatus);

		binder.setBean(new Merchant());

		return binder;
	}

	private Object registerNewAccount(SecurityService securityService, AuthorityRepository authorityRepository,
			Merchant bean) {
		getLogger().debug("Registering new account: " + bean.toString());

		Authorities roleAdmin = authorityRepository.findByAuthority("admin");

		@SuppressWarnings("serial")
		Set<Authorities> roles = new HashSet<Authorities>() {
			{
				add(roleAdmin);
			}
		};

		bean.setAuthorities(roles);

		boolean registrationSuccess = securityService.registerMember(bean);

		if (registrationSuccess) {

			final Window dialog = new Window("Registration sucessful. Please login.");
			dialog.setModal(true);
			dialog.setResizable(false);
			dialog.setDraggable(false);
			dialog.setWindowMode(WindowMode.NORMAL);

			getUI().addWindow(dialog);
			getUI().getNavigator().navigateTo("login");
		} else {
			Notification.show("Registration failed!", Notification.Type.ERROR_MESSAGE);
		}

		return null;
	}
}
