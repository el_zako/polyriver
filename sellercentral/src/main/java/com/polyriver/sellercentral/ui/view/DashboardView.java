package com.polyriver.sellercentral.ui.view;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import com.polyriver.app.repository.MerchantLayoutRepository;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.ui.component.DashboardLayout;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Composite;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;

@SpringView(name = "dashboard")
public class DashboardView extends Composite implements View, HasLogger {

	private static final long serialVersionUID = 6919045351627304881L;

	public DashboardView(MerchantLayoutRepository merchantLayoutRepository) {

		CssLayout menu = getSideMenu();

		InputStream design = getDesign(merchantLayoutRepository);

		HorizontalLayout mainLayout = new HorizontalLayout();

		//Chart chart = new TestChart();
		//mainLayout.addComponent(chart);

		DashboardLayout dashboard = new DashboardLayout(design);

		mainLayout.addComponent(menu, 0);
		mainLayout.addComponent(dashboard, 1);

		mainLayout.setExpandRatio(menu, 1);
		mainLayout.setExpandRatio(dashboard, 10);
		mainLayout.setSizeFull();

		setCompositionRoot(mainLayout);
		setResponsive(true);
	}

	private CssLayout getSideMenu() {
		Label title = new Label("Menu");
		title.addStyleName(ValoTheme.MENU_TITLE);

		Button view1 = new Button("View 1", e -> getUI().getNavigator().navigateTo("view1"));
		view1.addStyleNames(ValoTheme.BUTTON_LINK, ValoTheme.MENU_ITEM);

		Button view2 = new Button("View 2", e -> getUI().getNavigator().navigateTo("view2"));
		view2.addStyleNames(ValoTheme.BUTTON_LINK, ValoTheme.MENU_ITEM);

		Button logout = new Button("Sign out", e -> onLogout(e));
		logout.addStyleNames(ValoTheme.BUTTON_LINK, ValoTheme.MENU_ITEM);

		CssLayout menu = new CssLayout(title, view1, view2, logout);
		menu.addStyleName(ValoTheme.MENU_ROOT);

		return menu;
	}

	private InputStream getDesign(MerchantLayoutRepository merchantLayoutRepository) {

		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

		Optional<String> layout = merchantLayoutRepository.findByUserName(currentUser);

		String source;

		InputStream in = null;

		if (layout.isPresent()) {
			source = layout.get();
			getLogger().debug(layout.get().getClass().getName());
			getLogger().debug("User: " + currentUser);
			getLogger().debug("Design: " + source);

			try {
				in = IOUtils.toInputStream(source, "UTF-8");

			} catch (IOException e1) {
				getLogger().error("Error converting String to InputStream: " + source);
			}

		}

		return in;
	}

	private void onLogout(Button.ClickEvent event) {
		logout();
	}

	private void logout() {
		VaadinSession.getCurrent().close();
		Page.getCurrent().setLocation("");
	}

}
