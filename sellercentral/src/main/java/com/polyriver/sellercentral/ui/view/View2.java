package com.polyriver.sellercentral.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Composite;
import com.vaadin.ui.Label;

/**
 * Not implemented.
 * 
 * @author zarkasih.adlan
 *
 */
@SpringView(name = "view2")
public class View2 extends Composite implements View {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5950086864518016929L;

	public View2() {
        setCompositionRoot(new Label("This is view 2"));
    }
}