package com.polyriver.sellercentral.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Composite;
import com.vaadin.ui.Label;

/**
 * Not implemented.
 * 
 * @author zarkasih.adlan
 *
 */
@SpringView(name = "view1")
public class View1 extends Composite implements View {

    /**
	 * 
	 */
	private static final long serialVersionUID = 9109627306466747976L;

	public View1() {
        setCompositionRoot(new Label("This is view 1"));
    }
}
