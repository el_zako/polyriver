package com.polyriver.sellercentral.security.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.polyriver.app.model.Merchant;
import com.polyriver.app.model.PasswordResetToken;
import com.polyriver.app.repository.MerchantRepository;
import com.polyriver.app.repository.PasswordResetTokenRepository;
import com.polyriver.sellercentral.HasLogger;
import com.polyriver.sellercentral.security.SecurityService;
import com.vaadin.server.VaadinService;

@Service
public class SecurityServiceImpl implements SecurityService, HasLogger {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;

	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public String findLoggedInUsername() {
		Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
		if (userDetails instanceof UserDetails) {
			return ((UserDetails) userDetails).getUsername();
		}

		return null;
	}

	@Override
	public boolean login(String username, String password) {
		try {
			Authentication token = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(username, password));

			VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
			SecurityContextHolder.getContext().setAuthentication(token);

			return true;
		} catch (Exception ex) {
			getLogger().error("Error authenticating user: " + username, ex);
			return false;
		}
	}

	@Override
	public boolean registerMember(Merchant user) {
		try {
			Merchant existing = merchantRepository.findByUsername(user.getUsername());

			if (existing == null) {
				merchantRepository.save(user);

				getLogger().debug("Registered new user: " + user.getUsername());
				return true;
			} else {

				getLogger().error("User already exist: " + user.getUsername());
				return false;
			}
		} catch (Exception ex) {

			getLogger().error("Error registering user: " + user.getUsername(), ex);

			return false;
		}
	}

	@Override
	public boolean resetPassword(Long id, String newPassword) {
		try {
			Optional<Merchant> existing = merchantRepository.findById(id);

			if (existing.isPresent()) {
				existing.get().setPassword(newPassword);
				merchantRepository.save(existing.get());

				getLogger().debug("Reset password succeeded.");
				return true;
			} else {

				getLogger().error("User not found!");
				return false;
			}
		} catch (Exception ex) {

			getLogger().error("Error resetting password for id: " + id, ex);

			return false;
		}
	}

	@Override
	public void createPasswordResetTokenForUser(Merchant user, String token) {
		PasswordResetToken resetToken = new PasswordResetToken(user, token);
		passwordResetTokenRepository.save(resetToken);

		getLogger().debug("Created password reset token: " + resetToken + " for user " + user.getUsername());

	}
}
