package com.polyriver.sellercentral.security;

import com.polyriver.app.model.Merchant;

public interface SecurityService {
	
    String findLoggedInUsername();

    boolean login(String username, String password);
    
    boolean registerMember(Merchant user);
    
    boolean resetPassword(Long id, String newPassword);
    
    void createPasswordResetTokenForUser(Merchant user, String token);
    
}
