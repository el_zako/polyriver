package com.polyriver.sellercentral.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.polyriver.app.model.Merchant;
import com.polyriver.app.repository.MerchantRepository;
import com.polyriver.sellercentral.HasLogger;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, HasLogger {

    @Autowired
    private MerchantRepository merchantRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Merchant user = merchantRepository.findByUsername(username);

        getLogger().debug("Found user: " + user.toString());

        return user;
    }

}
