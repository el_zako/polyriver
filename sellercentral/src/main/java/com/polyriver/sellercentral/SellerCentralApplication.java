package com.polyriver.sellercentral;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.polyriver.app.model.Authorities;
import com.polyriver.app.model.Merchant;
import com.polyriver.app.model.MerchantLayout;
import com.polyriver.app.repository.AuthorityRepository;
import com.polyriver.app.repository.MerchantLayoutRepository;
import com.polyriver.app.repository.MerchantRepository;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan(basePackages = { "com.polyriver" })
@EnableJpaRepositories(basePackages = { "com.polyriver" })
@EntityScan(basePackages = { "com.polyriver" })
public class SellerCentralApplication implements CommandLineRunner, HasLogger {

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private AuthorityRepository authorityRepository;

	@Autowired
	private MerchantLayoutRepository merhcantLayoutRepository;

	public static void main(String[] args) {
		SpringApplication.run(SellerCentralApplication.class, args);
	}

	/**
	 * Start internal H2 server so we can query the DB from IDE
	 *
	 * @return H2 Server instance
	 * @throws SQLException
	 */
//	@Bean(initMethod = "start", destroyMethod = "stop")
//	public Server h2Server() throws SQLException {
//		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9999");
//	}

	@Override
	public void run(String... args) throws Exception {
		generateData(merchantRepository);
	}

	private void generateData(MerchantRepository userRepository) {
		if (userRepository.findByUsername("admin") == null) {
			Authorities admin = authorityRepository.save(new Authorities("admin"));
			authorityRepository.save(new Authorities("member"));

			@SuppressWarnings("serial")
			Set<Authorities> roles = new HashSet<Authorities>() {
				{
					add(admin);
				}
			};

			Merchant user1 = new Merchant("zadlan@mail.com", "password", roles);
			userRepository.save(user1);

			String testLayout1 = "<!doctype html>\r\n" + "<html>\r\n" + "	<head>\r\n"
					+ "		<meta charset=\"UTF-8\">\r\n"
					+ "			<meta name=\"design-properties\" content=\"{&quot;RULERS_VISIBLE&quot;:true,&quot;GUIDELINES_VISIBLE&quot;:false,&quot;SNAP_TO_OBJECTS&quot;:true,&quot;SNAP_TO_GRID&quot;:true,&quot;SNAPPING_DISTANCE&quot;:10,&quot;JAVA_SOURCES_ROOT&quot;:&quot;src/main/java&quot;,&quot;THEME&quot;:&quot;orderstheme&quot;}\">\r\n"
					+ "				<meta name=\"vaadin-version\" content=\"8.0.5\">\r\n"
					+ "					<meta name=\"package-mapping\" content=\"board:com.vaadin.board\">\r\n"
					+ "						<meta name=\"package-mapping\" content=\"chart:com.polyriver.sellercentral.ui.charts\">\r\n"
					+ "							<meta name=\"package-mapping\" content=\"pr:com.polyriver.sellercentral.ui.component\" />\r\n"
					+ "						</head>\r\n" + "						<body>\r\n"
					+ "							<vaadin-vertical-layout style-name=\"dashboard-view\" responsive size-full margin=\"false\">\r\n"
					+ "								<vaadin-label>\r\n"
					+ "									<b>Welcome new user!</b>\r\n"
					+ "								</vaadin-label>\r\n"
					+ "								<board-board id=\"board\" size-full _id=\"board\" :expand>\r\n"
					+ "									<board-row>\r\n"
					+ "										<chart-test-chart/>\r\n"
					+ "									</board-row>\r\n"
					+ "									<board-row>\r\n"
					+ "										<pr-order-table-component caption=\"Amazing Table\">\r\n"
					+ "											<vaadin-grid _id=\"ordergrid\" caption=\"Amazing Grid\"/>\r\n"
					+ "										</pr-order-table-component>			\r\n"
					+ "									</board-row>\r\n"
					+ "									<board-row>\r\n"
					+ "										<vaadin-label>\r\n"
					+ "											<b>Row 3 Column 1</b>\r\n"
					+ "										</vaadin-label>\r\n"
					+ "										<vaadin-label>\r\n"
					+ "											<b>Row 3 Column 2</b>\r\n"
					+ "										</vaadin-label>\r\n"
					+ "										<vaadin-label>\r\n"
					+ "											<b>Row 3 Column 3</b>\r\n"
					+ "										</vaadin-label>\r\n"
					+ "									</board-row>			\r\n"
					+ "								</board-board>\r\n"
					+ "							</vaadin-vertical-layout>\r\n" + "						</body>\r\n"
					+ "					</html>";

			String testLayout2 = "<!doctype html>\r\n" + "<html>\r\n" + " <head>\r\n" + "  <meta charset=\"UTF-8\">\r\n"
					+ "  <meta name=\"design-properties\" content=\"{&quot;RULERS_VISIBLE&quot;:true,&quot;GUIDELINES_VISIBLE&quot;:false,&quot;SNAP_TO_OBJECTS&quot;:true,&quot;SNAP_TO_GRID&quot;:true,&quot;SNAPPING_DISTANCE&quot;:10,&quot;JAVA_SOURCES_ROOT&quot;:&quot;src/main/java&quot;,&quot;THEME&quot;:&quot;orderstheme&quot;}\">\r\n"
					+ "  <meta name=\"vaadin-version\" content=\"8.0.5\">\r\n"
					+ "  <meta name=\"package-mapping\" content=\"board:com.vaadin.board\">\r\n" + " </head>\r\n"
					+ " <body>\r\n"
					+ "  <vaadin-vertical-layout style-name=\"dashboard-view\" responsive size-full margin=\"false\">\r\n"
					+ "   <board-board id=\"board\" size-full _id=\"board\" :expand>\r\n"
					+ "   			<board-row>\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 1 Column 1</b>\r\n" + "				</vaadin-label>\r\n"
					+ "				\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 1 Column 2</b>\r\n" + "				</vaadin-label>	\r\n" + "\r\n"
					+ "				<vaadin-label>\r\n" + "					<b>Row 1 Column 2</b>\r\n"
					+ "				</vaadin-label>							\r\n" + "			</board-row>\r\n"
					+ "			<board-row>\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 2 Column 1</b>\r\n" + "				</vaadin-label>\r\n"
					+ "							\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 2 Column 2</b>\r\n" + "				</vaadin-label>\r\n"
					+ "				\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 2 Column 3</b>\r\n" + "				</vaadin-label>				\r\n"
					+ "			</board-row>\r\n" + "			<board-row>\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 3 Column 1</b>\r\n" + "				</vaadin-label>\r\n"
					+ "				<vaadin-label>\r\n" + "					<b>Row 3 Column 2</b>\r\n"
					+ "				</vaadin-label>\r\n" + "				<vaadin-label>\r\n"
					+ "					<b>Row 3 Column 3</b>\r\n" + "				</vaadin-label>\r\n"
					+ "			</board-row>			\r\n" + "   </board-board>\r\n"
					+ "  </vaadin-vertical-layout>\r\n" + " </body>\r\n" + "</html>";

			merhcantLayoutRepository.save(new MerchantLayout(user1, testLayout1));

		}
	}

}
