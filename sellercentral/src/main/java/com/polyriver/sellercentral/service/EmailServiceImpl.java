package com.polyriver.sellercentral.service;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.polyriver.sellercentral.HasLogger;

@Component
public class EmailServiceImpl implements EmailService, HasLogger {

	@Autowired
	public JavaMailSender emailSender;

	@Override
	public boolean sendSimpleMessage(String to, String subject, String text) {
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text, true);

			emailSender.send(message);

			return true;
		} catch (Exception exception) {
			exception.printStackTrace();

			getLogger().error("Sending email to " + to + " failed.", exception);

			return false;
		}
	}

}
