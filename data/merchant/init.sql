create table public.authorities (id  bigserial not null, version int4 not null, authority varchar(255), primary key (id))
create table public.merchant (id  bigserial not null, version int4 not null, account_non_expired boolean not null, account_non_locked boolean not null, credentials_non_expired boolean not null, customer_name varchar(255), enabled boolean not null, password varchar(255), email varchar(255), primary key (id))
create table public.merchant_authorities (merchant_id int8 not null, authorities_id int8 not null, primary key (merchant_id, authorities_id))
create table public.merchant_layout (id  bigserial not null, version int4 not null, layout varchar(9999), primary key (id))
create table public.password_reset_token (id  bigserial not null, version int4 not null, expiry_date timestamp, token varchar(255), merchant_id int8 not null, primary key (id))
alter table public.merchant add constraint UK_22hw5xdmw9ehbp92kr3h9pbh unique (email)
alter table public.merchant_authorities add constraint FKerp4av3bo1o1drf78radby3vx foreign key (authorities_id) references public.authorities
alter table public.merchant_authorities add constraint FKq6xw04muicacusyde8umw9vrh foreign key (merchant_id) references public.merchant
alter table public.password_reset_token add constraint FKeobw8mvvp14w3ulkt2qdxfa26 foreign key (merchant_id) references public.merchant
