package com.polyriver.polyriver.app.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.polyriver.app.PolyriverMerchantDataApplication;
import com.polyriver.app.model.Merchant;
import com.polyriver.app.repository.MerchantRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = PolyriverMerchantDataApplication.class)
public class MerchantRepositoryTest {

	@Autowired
	MerchantRepository repo;

	@Test
	public void testMerchantRepoEmpty() throws Exception {
		List<Merchant> results = repo.findAll();
		System.out.println(results.toString());
		assertThat(results).isEmpty();
	}

	@Test
	public void testInsertMerchantRepo() {
		Merchant me = new Merchant("Z Adlan", "zadlan@gmail.com", "qwerty", null, false, false, false, true, null);
		repo.save(me);

		List<Merchant> results = repo.findAll();
		for (Merchant result : results) {
			System.out.println(result.toString());
		}

		assertThat(results).isNotEmpty();
	}
}
