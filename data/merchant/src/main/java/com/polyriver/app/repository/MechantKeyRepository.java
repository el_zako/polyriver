package com.polyriver.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polyriver.app.model.MerchantKey;

public interface MechantKeyRepository extends JpaRepository<MerchantKey, Long> {

}
