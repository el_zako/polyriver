package com.polyriver.app.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "authorities")
public class Authorities extends AbstractEntity implements GrantedAuthority {
   
    private static final long serialVersionUID = -6376173997495465449L;

    private String authority;

    public Authorities() {
        // Empty constructor is needed by Spring Data / JPA
    }
    
    public Authorities(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
