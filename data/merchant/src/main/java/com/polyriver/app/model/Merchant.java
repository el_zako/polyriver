package com.polyriver.app.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.polyriver.util.SecurityUtils;

@Entity
@Table(name = "merchant")
public class Merchant extends AbstractEntity implements UserDetails {

	private static final long serialVersionUID = 951705223487128402L;

	private String customerName;

	/**
	 * Email address as login id (principal)
	 */
	@Column(name = "email", unique = true)
	private String username;

	private String password;

	@ManyToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	private Set<Authorities> authorities;

	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled = true;

	@OneToMany(mappedBy = "merchant", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MerchantKey> merchantKeys = new ArrayList<>();

	public Merchant() {
		// Empty constructor is needed by Spring Data / JPA
	}

	public Merchant(String username, String password, Set<Authorities> authorities) {
		super();
		this.username = username;
		this.password = SecurityUtils.hashPassword(password);
		this.authorities = authorities;
	}

	public Merchant(String customerName, String username, String password, Set<Authorities> authorities,
			boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled,
			List<MerchantKey> merchantKeys) {
		super();
		this.customerName = customerName;
		this.username = username;
		this.password = SecurityUtils.hashPassword(password);
		this.authorities = authorities;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
		this.enabled = enabled;
		this.merchantKeys = merchantKeys;
	}

	public void addMerchantKey(MerchantKey key) {
		merchantKeys.add(key);
		key.setMerchant(this);
	}

	public void removeMerchantKey(MerchantKey key) {
		merchantKeys.remove(key);
		key.setMerchant(null);
	}

	public String getCustomerName() {
		return customerName;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setPassword(String password) {
		this.password = SecurityUtils.hashPassword(password);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setAuthorities(Set<Authorities> authorities) {
		this.authorities = authorities;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<MerchantKey> getMerchantKeys() {
		return merchantKeys;
	}

	public void setMerchantkeys(List<MerchantKey> merchantKeys) {
		this.merchantKeys = merchantKeys;
	}

	@Override
	public String toString() {
		return "Merchant [customerName=" + customerName + ", username=" + username + ", password=" + password
				+ ", authorities=" + authorities + ", accountNonExpired=" + accountNonExpired + ", accountNonLocked="
				+ accountNonLocked + ", credentialsNonExpired=" + credentialsNonExpired + ", enabled=" + enabled
				+ ", merchantKeys=" + merchantKeys + "]";
	}

}
