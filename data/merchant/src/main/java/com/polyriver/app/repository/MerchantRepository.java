package com.polyriver.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polyriver.app.model.Merchant;

public interface MerchantRepository extends JpaRepository<Merchant, Long> {

	Merchant findByUsername(String userName);

}
