package com.polyriver.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.polyriver.app.model.MerchantLayout;

public interface MerchantLayoutRepository extends JpaRepository<MerchantLayout, Long> {

	//@Query("SELECT u.layout FROM merchant_layout u WHERE u.merchant.username = :name")
	@Query("SELECT ml.layout FROM MerchantLayout ml WHERE ml.merchant.username = :name")
	public Optional<String> findByUserName(@Param("name") String userName);
}
