package com.polyriver.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.polyriver.app.model.Authorities;

public interface AuthorityRepository extends JpaRepository<Authorities, Long> {

    Authorities findByAuthority(String authority);

}
