package com.polyriver.app.model;


import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "password_reset_token")
public class PasswordResetToken extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4358723771946427334L;

	private String token;

	@OneToOne(targetEntity = Merchant.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "merchant_id")
	private Merchant merchant;

	private LocalDateTime expiryDate;

	public PasswordResetToken() {
		super();
	}

	public PasswordResetToken(Merchant merchant, String token) {
		super();
		this.merchant = merchant;
		this.token = token;
		this.expiryDate = LocalDateTime.now().plusMinutes(10);
	}
	
	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}



	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "PasswordResetToken [token=" + token + ", merchant=" + merchant + ", expiryDate=" + expiryDate + "]";
	}



}
