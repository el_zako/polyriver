package com.polyriver.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "merchant_layout")
public class MerchantLayout extends AbstractEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5162197829198825744L;

	@OneToOne
	@JoinColumn(name = "merchant_id")
	private Merchant merchant;

	// TODO consider using blob here
	@Column(length = 9999)
	private String layout;

	public MerchantLayout() {
		// Empty constructor is needed by Spring Data / JPA
	}

	public MerchantLayout(Merchant merchant, String layout) {
		super();
		this.merchant = merchant;
		this.layout = layout;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

}
