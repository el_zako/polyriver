package com.polyriver.amazon.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Self generated Order object based on response from MWS.
 * 
 * @author zarkasih.adlan
 *
 */
@XmlRootElement(name = "Order")
@Entity(name = "order_table")
public class Order {

	@Id
	private String amazonOrderId;

	private String merchantOrderID;

	@Transient
	private XMLGregorianCalendar purchaseDate;

	@Transient
	private XMLGregorianCalendar lastUpdateDate;

	private String orderStatus;

	private String salesChannel;

	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private FulfillmentData fulfillmentData;

	private Boolean isBusinessOrder;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "order")
	private List<OrderItem> orderItem;

	public Order() {
		super();
	}

	public Order(String amazonOrderId, String merchantOrderID, XMLGregorianCalendar purchaseDate,
			XMLGregorianCalendar lastUpdateDate, String orderStatus, String salesChannel,
			FulfillmentData fulfillmentData, Boolean isBusinessOrder, List<OrderItem> orderItem) {
		super();
		this.amazonOrderId = amazonOrderId;
		this.merchantOrderID = merchantOrderID;
		this.purchaseDate = purchaseDate;
		this.lastUpdateDate = lastUpdateDate;
		this.orderStatus = orderStatus;
		this.salesChannel = salesChannel;
		this.fulfillmentData = fulfillmentData;
		this.isBusinessOrder = isBusinessOrder;
		this.orderItem = orderItem;
	}

	@XmlElement(name = "AmazonOrderID")
	public String getAmazonOrderId() {
		return amazonOrderId;
	}

	public void setAmazonOrderId(String amazonOrderId) {
		this.amazonOrderId = amazonOrderId;
	}

	@XmlElement(name = "MerchantOrderID")
	public String getMerchantOrderID() {
		return merchantOrderID;
	}

	public void setMerchantOrderID(String merchantOrderID) {
		this.merchantOrderID = merchantOrderID;
	}

	@XmlElement(name = "PurchaseDate")
	public XMLGregorianCalendar getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(XMLGregorianCalendar purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@XmlElement(name = "LastUpdatedDate")
	public XMLGregorianCalendar getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(XMLGregorianCalendar lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	@XmlElement(name = "OrderStatus")
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@XmlElement(name = "SalesChannel")
	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	@XmlElement(name = "FulfillmentData")
	public FulfillmentData getFulfillmentData() {
		return fulfillmentData;
	}

	public void setFulfillmentData(FulfillmentData fulfillmentData) {
		this.fulfillmentData = fulfillmentData;
	}

	@XmlElement(name = "IsBusinessOrder")
	public Boolean getIsBusinessOrder() {
		return isBusinessOrder;
	}

	public void setIsBusinessOrder(Boolean isBusinessOrder) {
		this.isBusinessOrder = isBusinessOrder;
	}

	@XmlElement(name = "OrderItem")
	public List<OrderItem> getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(List<OrderItem> orderItem) {
		this.orderItem = orderItem;
	}

	@Override
	public String toString() {
		return "Order [amazonOrderId=" + amazonOrderId + ", merchantOrderID=" + merchantOrderID + ", purchaseDate="
				+ purchaseDate + ", lastUpdateDate=" + lastUpdateDate + ", orderStatus=" + orderStatus
				+ ", salesChannel=" + salesChannel + ", fulfillmentData=" + fulfillmentData + ", isBusinessOrder="
				+ isBusinessOrder + ", orderItem=" + orderItem + "]";
	}

}
