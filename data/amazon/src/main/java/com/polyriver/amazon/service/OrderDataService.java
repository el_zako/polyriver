package com.polyriver.amazon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.polyriver.amazon.model.Order;
import com.polyriver.amazon.repository.OrderRepository;

@Service
public class OrderDataService {

	@Autowired
	private OrderRepository orderRepository;

	public OrderDataService() {
		super();
	}

	public Iterable<Order> findAll() {

		return orderRepository.findAll();
	}

}
