package com.polyriver.amazon.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OrderItem")
@Entity(name = "order_item")
public class OrderItem {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "amazonOrderId")
	private Order order;

	private String asin;

	private String sku;

	private String itemStatus;

	private String productName;

	private Integer quantity;

	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private ItemPrice itemPrice;

	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy="orderItem")
	private Promotion promotion;

	public OrderItem() {
		super();
	}

	public OrderItem(String asin, String sku, String itemStatus, String productName, Integer quantity,
			ItemPrice itemPrice) {
		super();
		this.asin = asin;
		this.sku = sku;
		this.itemStatus = itemStatus;
		this.productName = productName;
		this.quantity = quantity;
		this.itemPrice = itemPrice;
	}

	@XmlElement(name = "ASIN")
	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	@XmlElement(name = "SKU")
	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	@XmlElement(name = "ItemStatus")
	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	@XmlElement(name = "ProductName")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@XmlElement(name = "Quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@XmlElement(name = "ItemPrice")
	public ItemPrice getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(ItemPrice itemPrice) {
		this.itemPrice = itemPrice;
	}

	@XmlElement(name = "Promotion")
	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "OrderItem [asin=" + asin + ", sku=" + sku + ", itemStatus=" + itemStatus + ", productName="
				+ productName + ", quantity=" + quantity + ", itemPrice=" + itemPrice + "]";
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void afterUnmarshal(Unmarshaller u, Object parent) {
		this.order = ((Order) parent);
	}

	// TODO investigate query performance of bidirectional @OneToMany

	// @ManyToOne
	// @JoinColumn(name = "amazonOrderId")
	// private Order order;

	// public void afterUnmarshal(Unmarshaller u, Object parent) {
	// this.order = ((Order) parent);
	// }
	//
	// public Order getOrder() {
	// return order;
	// }
	//
	// public void setOrder(Order order) {
	// this.order = order;
	// }

}
