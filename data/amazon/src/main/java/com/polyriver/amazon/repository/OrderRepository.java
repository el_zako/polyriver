package com.polyriver.amazon.repository;

import org.springframework.data.repository.CrudRepository;

import com.polyriver.amazon.model.Order;

public interface OrderRepository extends CrudRepository<Order, String> {

}
