package com.polyriver.amazon.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Promotion")
@Entity(name = "promotion")
public class Promotion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "orderItemId")
	private OrderItem orderItem;

	private String promotionIds;

	private String shipPromotionDiscount;

	private String itemPromotionDiscount;

	@XmlElement(name = "PromotionIDs")
	public String getPromotionIds() {
		return promotionIds;
	}
	
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		this.orderItem = ((OrderItem) parent);
	}

	public void setPromotionIds(String promotionIds) {
		this.promotionIds = promotionIds;
	}

	@XmlElement(name = "ShipPromotionDiscount")
	public String getShipPromotionDiscount() {
		return shipPromotionDiscount;
	}

	public void setShipPromotionDiscount(String shipPromotionDiscount) {
		this.shipPromotionDiscount = shipPromotionDiscount;
	}

	@XmlElement(name = "ItemPromotionDiscount")
	public String getItemPromotionDiscount() {
		return itemPromotionDiscount;
	}

	public void setItemPromotionDiscount(String itemPromotionDiscount) {
		this.itemPromotionDiscount = itemPromotionDiscount;
	}

	public OrderItem getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}

}
