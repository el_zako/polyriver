package com.polyriver.amazon.model;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FulfillmentData")
@Entity(name = "fulfillment_data")
public class FulfillmentData {

	@Id
	private String amazonOrderId;

	private String fulfillmentChannel;

	private String shipServiceLevel;

	@Embedded
	private Address address;

	public FulfillmentData() {
		super();
	}

	public FulfillmentData(String fulfillmentChannel, String shipServiceLevel, Address address) {
		super();
		this.fulfillmentChannel = fulfillmentChannel;
		this.shipServiceLevel = shipServiceLevel;
		this.address = address;
	}

	public void afterUnmarshal(Unmarshaller u, Object parent) {
		this.amazonOrderId = ((Order) parent).getAmazonOrderId();
	}

	@XmlElement(name = "FulfillmentChannel")
	public String getFulfillmentChannel() {
		return fulfillmentChannel;
	}

	public void setFulfillmentChannel(String fulfillmentChannel) {
		this.fulfillmentChannel = fulfillmentChannel;
	}

	@XmlElement(name = "ShipServiceLevel")
	public String getShipServiceLevel() {
		return shipServiceLevel;
	}

	public void setShipServiceLevel(String shipServiceLevel) {
		this.shipServiceLevel = shipServiceLevel;
	}

	@XmlElement(name = "Address")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getAmazonOrderId() {
		return amazonOrderId;
	}

	public void setAmazonOrderId(String amazonOrderId) {
		this.amazonOrderId = amazonOrderId;
	}

	@Override
	public String toString() {
		return "FulfillmentData [amazonOrderId=" + amazonOrderId + ", fulfillmentChannel=" + fulfillmentChannel
				+ ", shipServiceLevel=" + shipServiceLevel + ", address=" + address + "]";
	}

	@XmlRootElement(name = "Address")
	static class Address implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3314744211170696027L;

		private String city;

		private String state;

		private String postalCode;

		private String country;

		public Address() {
			super();
		}

		@XmlElement(name = "City")
		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		@XmlElement(name = "State")
		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		@XmlElement(name = "PostalCode")
		public String getPostalCode() {
			return postalCode;
		}

		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}

		@XmlElement(name = "Country")
		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		@Override
		public String toString() {
			return "Address [city=" + city + ", state=" + state + ", postalCode=" + postalCode + ", country=" + country
					+ "]";
		}

	}

}
