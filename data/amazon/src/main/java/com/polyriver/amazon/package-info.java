/**
 * Package for Amazon Marketplace Web Service (Amazon MWS) related entities.
 */
/**
 * @author zarkasih.adlan
 *
 */
package com.polyriver.amazon;