package com.polyriver.amazon.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "Component")
@Entity(name = "component")
public class Component {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long componentId;

	private String type;

	@Embedded
	private Amount amount;

	@ManyToOne
	@JoinColumn(name = "itemPriceId")
	private ItemPrice itemPrice;

	public Component() {
		super();
	}

	public Component(String type, Amount amount) {
		super();
		this.type = type;
		this.amount = amount;

	}

	@XmlElement(name = "Type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name = "Amount")
	public Amount getAmount() {
		return amount;
	}

	public void setAmount(Amount amount) {
		this.amount = amount;
	}

	public Long getComponentId() {
		return componentId;
	}

	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	@Override
	public String toString() {
		return "Component [type=" + type + ", amount=" + amount + "]";
	}

	@XmlRootElement(name = "Amount")
	static class Amount {

		private String amount;

		private String currency;

		public Amount() {
			super();
		}

		@XmlValue
		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		@XmlAttribute(name = "currency")
		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		@Override
		public String toString() {
			return "Amount [amount=" + amount + ", currency=" + currency + "]";
		}

	}
	
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		this.itemPrice = ((ItemPrice) parent);
	}

	public ItemPrice getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(ItemPrice itemPrice) {
		this.itemPrice = itemPrice;
	}

}
