package com.polyriver.amazon.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ItemPrice")
@Entity(name = "item_price")
public class ItemPrice {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// @OneToOne
	// private OrderItem orderItem;
	//
	// public void afterUnmarshal(Unmarshaller u, Object parent) {
	// this.orderItem = ((OrderItem) parent);
	// }

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY , mappedBy="itemPrice")
	private List<Component> component;

	public ItemPrice() {
		super();
	}


	@XmlElement(name = "Component")
	public List<Component> getComponent() {
		return component;
	}

	public void setComponent(List<Component> component) {
		this.component = component;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ItemPrice [component=" + component + "]";
	}

	// public OrderItem getOrderItem() {
	// return orderItem;
	// }
	//
	// public void setOrderItem(OrderItem orderItem) {
	// this.orderItem = orderItem;
	// }

}
