package com.polyriver.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public final class SecurityUtils {

    public static String hashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        return hashedPassword;
    }


}
