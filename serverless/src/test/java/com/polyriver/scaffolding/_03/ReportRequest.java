package com.polyriver.scaffolding._03;

import javax.xml.datatype.XMLGregorianCalendar;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.ReportRequestInfo;
import com.amazonaws.mws.model.RequestReportRequest;
import com.amazonaws.mws.model.RequestReportResponse;
import com.amazonaws.mws.model.RequestReportResult;
import com.amazonaws.mws.model.ResponseMetadata;

public class ReportRequest {

	private static ReportRequest instance;

	private ReportRequest() {

	}

	public static ReportRequest getInstance() {
		if (instance == null) {
			instance = new ReportRequest();
		}
		return instance;
	}

	public String requestReport(String merchantId, MarketplaceWebService service, XMLGregorianCalendar startDate,
			XMLGregorianCalendar endDate) {

		String reportRequestId = null;

		RequestReportRequest request = new RequestReportRequest().withMerchant(merchantId)
				.withReportType("_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_")
				.withReportOptions("ShowSalesChannel=true").withStartDate(startDate).withEndDate(endDate);


		RequestReportResponse requestReportResponse = invokeRequestReport(service, request);
		if (requestReportResponse != null) {
			reportRequestId = requestReportResponse.getRequestReportResult().getReportRequestInfo()
					.getReportRequestId();
			
			System.out.println(requestReportResponse.toXML());
		}

		return reportRequestId;
	}

	/**
	 * Request Report request sample requests the generation of a report
	 * 
	 * @param service
	 *            instance of MarketplaceWebService service
	 * @param request
	 *            Action to invoke
	 */
	public RequestReportResponse invokeRequestReport(MarketplaceWebService service, RequestReportRequest request) {
		try {

			RequestReportResponse response = service.requestReport(request);

			System.out.println("RequestReport Action Response");
			System.out.println("=============================================================================");
			System.out.println("/n");

			System.out.println("    RequestReportResponse");
			System.out.println("/n");
			if (response.isSetRequestReportResult()) {
				System.out.println("        RequestReportResult");
				System.out.println("/n");
				RequestReportResult requestReportResult = response.getRequestReportResult();
				if (requestReportResult.isSetReportRequestInfo()) {
					System.out.println("            ReportRequestInfo");
					System.out.println("/n");
					ReportRequestInfo reportRequestInfo = requestReportResult.getReportRequestInfo();
					if (reportRequestInfo.isSetReportRequestId()) {
						System.out.println("                ReportRequestId");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getReportRequestId());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetReportType()) {
						System.out.println("                ReportType");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getReportType());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetStartDate()) {
						System.out.println("                StartDate");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getStartDate());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetEndDate()) {
						System.out.println("                EndDate");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getEndDate());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetSubmittedDate()) {
						System.out.println("                SubmittedDate");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getSubmittedDate());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetReportProcessingStatus()) {
						System.out.println("                ReportProcessingStatus");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getReportProcessingStatus());
						System.out.println("/n");
					}
				}
			}
			if (response.isSetResponseMetadata()) {
				System.out.println("        ResponseMetadata");
				System.out.println("/n");
				ResponseMetadata responseMetadata = response.getResponseMetadata();
				if (responseMetadata.isSetRequestId()) {
					System.out.println("            RequestId");
					System.out.println("/n");
					System.out.println("                " + responseMetadata.getRequestId());
					System.out.println("/n");
				}
			}
			System.out.println("/n");
			System.out.println(response.getResponseHeaderMetadata().toString());
			System.out.println("/n");

			return response;

		} catch (MarketplaceWebServiceException ex) {

			System.out.println("Caught Exception: " + ex.getMessage());
			System.out.println("Response Status Code: " + ex.getStatusCode());
			System.out.println("Error Code: " + ex.getErrorCode());
			System.out.println("Error Type: " + ex.getErrorType());
			System.out.println("Request ID: " + ex.getRequestId());
			System.out.println("XML: " + ex.getXML());
			System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
		}

		return null;
	}

}
