package com.polyriver.scaffolding._02;

import java.util.Map;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

@SuppressWarnings("rawtypes")
public class MWSClient implements RequestHandler<Map, Object> {

	private static MWSClient instance;

	static final String accessKeyId = "AKIAJZ473XQFTEHGQQWQ";
	static final String secretAccessKey = "";

	static final String appName = "Polyriver";
	static final String appVersion = "0.1";

	static MarketplaceWebServiceConfig config;

	static MarketplaceWebService service;

	private MWSClient() {

	}

	@Override
	public Object handleRequest(Map input, Context context) {
		// TODO Get accessKeyId and secretAccessKey from database based on merchantId

		String merchantId = input.get("merchantId").toString();
		getMerchantMetadata(merchantId);

		service = new MarketplaceWebServiceClient(accessKeyId, secretAccessKey, appName, appVersion, config);

		return service;
	}

	private void getMerchantMetadata(String merchantId) {
		// TODO get merchant metadata from database

	}

	public static MWSClient getInstance() {
		if (instance == null) {
			instance = new MWSClient();
			config = new MarketplaceWebServiceConfig();
			config.setServiceURL("https://mws.amazonservices.com/");
			service = new MarketplaceWebServiceClient(accessKeyId, secretAccessKey, appName, appVersion, config);

		}
		return instance;
	}

	public static MarketplaceWebServiceConfig getConfig() {
		return config;
	}

	public static void setConfig(MarketplaceWebServiceConfig config) {
		MWSClient.config = config;
	}

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public String getSecretAccessKey() {
		return secretAccessKey;
	}

	public String getAppName() {
		return appName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public MarketplaceWebService getService() {
		return service;
	}

	@Override
	public String toString() {
		return "GetMWSClient [accessKeyId=" + accessKeyId + ", secretAccessKey=" + secretAccessKey + ", appName="
				+ appName + ", appVersion=" + appVersion + "]";
	}

}
