package com.polyriver.scaffolding._00;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.postgresql.ds.PGSimpleDataSource;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.polyriver.app.model.Merchant;

public class PutMerchantDataInS3Test {

	static String clientRegion = "ap-southeast-1";
	static String bucketName = "merchant-metadata-dev";

	static String accessKey = "AKIAITBKZDXSF46MEPEA";
	static String secretKey = "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";

	public static void main(String[] args) {
		BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();

		try {

			PGSimpleDataSource dataSource = new PGSimpleDataSource();
			dataSource.setServerName("localhost");
			dataSource.setDatabaseName("postgres");
			dataSource.setCurrentSchema("public");
			dataSource.setUser("pdev");
			dataSource.setPassword("polyriver");
			dataSource.setPortNumber(5432);

			QueryRunner runner = new QueryRunner(dataSource);

			ResultSetHandler<List<Merchant>> m = new BeanListHandler<Merchant>(Merchant.class);

			StringBuilder query = new StringBuilder();
			query.append(
					"SELECT m.id,m.email,mk.account_id,mk.access_key,mk.secret_key,mk.endpoint, mk.marketplace_name ")
					.append("FROM merchant m ").append("LEFT JOIN merchant_key mk ").append("ON m.id=mk.merchant_id ")
					.append("WHERE m.enabled='true' AND mk.enabled='true'");

			List<Merchant> merchants = runner.query(query.toString(), m);

			for (Merchant merchantDetails : merchants) {
				// logger.info("Processing merchant: " + merchantDetails.toString());
				String key = merchantDetails.getId() + "/" + merchantDetails.getMarketplace_name();
				s3Client.putObject(bucketName, key, merchantDetails.toString());
			}

		} catch (SQLException e) {
			// logger.error(e);
		}
	}
}
