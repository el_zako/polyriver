package com.polyriver.scaffolding._00;

import com.polyriver.scaffolding._01.MerchantId;
import com.polyriver.scaffolding._02.MWSClient;
import com.polyriver.scaffolding._05.Report;

/**
 * Function scaffolding to be ported over to serverless architecture. Each step
 * is one function.
 * 
 * @author zarkasih.adlan
 *
 */
public class StepFunctionsLocalTest {

	public static void main(String[] args) throws Exception {
		System.out.println("Started Step Functions");

		/**
		 * Step 1: Get MerchantId from user database
		 */
		 MerchantId mid = MerchantId.getInstance();
		 System.out.println(mid.toString());

		/**
		 * Step 2: Create MWSClient based on MerchantId
		 */
		 MWSClient gmwsc = MWSClient.getInstance();
		 System.out.println(gmwsc.toString());

		/**
		 * Step 3: Call ReportRequest, get ReportRequestId
		 */
//		 ReportRequest reportRequest = ReportRequest.getInstance();
//		 DatatypeFactory df = DatatypeFactory.newInstance();
//		 XMLGregorianCalendar startDate = df.newXMLGregorianCalendar(new
//		 GregorianCalendar(2018, 7, 1));
//		 XMLGregorianCalendar endDate = df.newXMLGregorianCalendar(new
//		 GregorianCalendar(2018, 7, 31));
//		 reportRequest.requestReport(mid.getMerchantId(), gmwsc.getService(),
//		 startDate, endDate);

		/**
		 * Step 4: Call ReportListRequest with ReportRequestId, get ReportId
		 */
//		 ReportListRequest reportList = ReportListRequest.getInstance();
//		 IdList idList = new IdList();
//		 idList.setId(Collections.singletonList("54789017769"));
//		 reportList.getReportList(mid.getMerchantId(), gmwsc.getService(), idList);

		/**
		 * Step 5: Call Report with ReportId, get report output stream with payload
		 */
		 Report report = Report.getInstance();
		 report.getReport(mid.getMerchantId(), gmwsc.getService(),
		 "10980252924017769");

	}

}
