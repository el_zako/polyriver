package com.polyriver.scaffolding._05;

import java.io.ByteArrayOutputStream;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.GetReportRequest;
import com.amazonaws.mws.model.GetReportResponse;
import com.amazonaws.mws.model.ResponseMetadata;

public class Report {

	private static Report instance;

	private String reportId;

	private Report() {
	}

	public static Report getInstance() {
		if (instance == null) {
			instance = new Report();
		}
		return instance;
	}

	public void getReport(String merchantId, MarketplaceWebService service, String reportId) {

		GetReportRequest request = new GetReportRequest();
		request.setMerchant(merchantId);
		// request.setMWSAuthToken(sellerDevAuthToken);

		request.setReportId(reportId);

		// Note that depending on the type of report being downloaded, a report can
		// reach
		// sizes greater than 1GB. For this reason we recommend that you _always_
		// program to
		// MWS in a streaming fashion. Otherwise, as your business grows you may
		// silently reach
		// the in-memory size limit and have to re-work your solution.
		//

		ByteArrayOutputStream report = new ByteArrayOutputStream();
		request.setReportOutputStream(report);

		GetReportResponse reportResponse = invokeGetReport(service, request);

		// InputStream inputStream = new ByteArrayInputStream(report.toByteArray());

		System.out.println(reportResponse.toXML());

	}

	/**
	 * Get Report request sample The GetReport operation returns the contents of a
	 * report. Reports can potentially be very large (>100MB) which is why we only
	 * return one report at a time, and in a streaming fashion.
	 * 
	 * @param service
	 *            instance of MarketplaceWebService service
	 * @param request
	 *            Action to invoke
	 */
	public GetReportResponse invokeGetReport(MarketplaceWebService service, GetReportRequest request) {
		GetReportResponse response = null;

		try {

			response = service.getReport(request);

			System.out.println("GetReport Action Response");
			System.out.println("=============================================================================");
			System.out.println("/n");

			System.out.println("    GetReportResponse");
			System.out.println("/n");
			System.out.println("    GetReportResult");
			System.out.println("/n");
			System.out.println("            MD5Checksum");
			System.out.println("/n");
			System.out.println("                " + response.getGetReportResult().getMD5Checksum());
			System.out.println("/n");
			if (response.isSetResponseMetadata()) {
				System.out.println("        ResponseMetadata");
				System.out.println("/n");
				ResponseMetadata responseMetadata = response.getResponseMetadata();
				if (responseMetadata.isSetRequestId()) {
					System.out.println("            RequestId");
					System.out.println("/n");
					System.out.println("                " + responseMetadata.getRequestId());
					System.out.println("/n");
				}
			}
			System.out.println("/n");

			System.out.println("Report");
			System.out.println("=============================================================================");
			System.out.println("/n");
			System.out.println(request.getReportOutputStream().toString());
			System.out.println("/n");

			System.out.println(response.getResponseHeaderMetadata().toString());
			System.out.println("/n");

		} catch (MarketplaceWebServiceException ex) {

			System.out.println("Caught Exception: " + ex.getMessage());
			System.out.println("Response Status Code: " + ex.getStatusCode());
			System.out.println("Error Code: " + ex.getErrorCode());
			System.out.println("Error Type: " + ex.getErrorType());
			System.out.println("Request ID: " + ex.getRequestId());
			System.out.println("XML: " + ex.getXML());
			System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
		}
		return response;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	@Override
	public String toString() {
		return "Report [reportId=" + reportId + "]";
	}

}
