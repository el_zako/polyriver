package com.polyriver.scaffolding._01;

import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * This function will be invoked hourly by CloudWatch. 
 * 
 * Sample query: select * from merchant where subscriptionStatus = "paid"
 * 
 * @author zarkasih.adlan
 *
 */
@SuppressWarnings("rawtypes")
public class MerchantId implements RequestHandler<Map, String> {

	private static MerchantId instance;

	private String merchantId = "AJ6OIY87NDH0U";
	

	private MerchantId() {
	}

	@Override
	public String handleRequest(Map input, Context context) {
		context.getLogger().log("Returning merchant id: " + merchantId);
		


		return merchantId;
	}

	public static MerchantId getInstance() {
		if (instance == null) {
			instance = new MerchantId();
		}
		return instance;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	@Override
	public String toString() {
		return "GetMerchantId [merchantId=" + merchantId + "]";
	}

}
