package com.polyriver.scaffolding._04;

import java.util.List;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.GetReportListRequest;
import com.amazonaws.mws.model.GetReportListResponse;
import com.amazonaws.mws.model.GetReportListResult;
import com.amazonaws.mws.model.IdList;
import com.amazonaws.mws.model.ReportInfo;
import com.amazonaws.mws.model.ResponseMetadata;

public class ReportListRequest {

	private static ReportListRequest instance;

	private ReportListRequest() {
	}

	public static ReportListRequest getInstance() {
		if (instance == null) {
			instance = new ReportListRequest();
		}
		return instance;
	}

	public void getReportList(String merchantId, MarketplaceWebService service, IdList reportRequestIdList) {
		GetReportListRequest request = new GetReportListRequest();
		request.setMerchant(merchantId);

		// request.setReportTypeList(new TypeList(Arrays.asList("_GET_MERCHANT_LISTINGS_ALL_DATA_")));
		//request.withReportTypeList(new TypeList(Arrays.asList("_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_")));
		request.setReportRequestIdList(reportRequestIdList);
		
		
		GetReportListResponse response = invokeGetReportList(service, request);
		System.out.println(response.toXML());
		
		
		GetReportListResult result = response.getGetReportListResult();
		List<ReportInfo> reportInfo = result.getReportInfoList();
		
		for (ReportInfo info : reportInfo) {			
			System.out.println(info.getReportId());
		}
		
		
	}

	/**
	 * Get Report List request sample returns a list of reports; by default the most
	 * recent ten reports, regardless of their acknowledgement status
	 * 
	 * @param service
	 *            instance of MarketplaceWebService service
	 * @param request
	 *            Action to invoke
	 */
	public GetReportListResponse invokeGetReportList(MarketplaceWebService service, GetReportListRequest request) {
		try {

			GetReportListResponse response = service.getReportList(request);

			System.out.println("GetReportList Action Response");
			System.out.println("=============================================================================");
			System.out.println("/n");

			System.out.println("    GetReportListResponse");
			System.out.println("/n");
			if (response.isSetGetReportListResult()) {
				System.out.println("        GetReportListResult");
				System.out.println("/n");
				GetReportListResult getReportListResult = response.getGetReportListResult();
				if (getReportListResult.isSetNextToken()) {
					System.out.println("            NextToken");
					System.out.println("/n");
					System.out.println("                " + getReportListResult.getNextToken());
					System.out.println("/n");
				}
				if (getReportListResult.isSetHasNext()) {
					System.out.println("            HasNext");
					System.out.println("/n");
					System.out.println("                " + getReportListResult.isHasNext());
					System.out.println("/n");
				}
				java.util.List<ReportInfo> reportInfoListList = getReportListResult.getReportInfoList();
				for (ReportInfo reportInfoList : reportInfoListList) {
					System.out.println("            ReportInfoList");
					System.out.println("/n");
					if (reportInfoList.isSetReportId()) {
						System.out.println("                ReportId");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getReportId());
						System.out.println("/n");
					}
					if (reportInfoList.isSetReportType()) {
						System.out.println("                ReportType");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getReportType());
						System.out.println("/n");
					}
					if (reportInfoList.isSetReportRequestId()) {
						System.out.println("                ReportRequestId");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getReportRequestId());
						System.out.println("/n");
					}
					if (reportInfoList.isSetAvailableDate()) {
						System.out.println("                AvailableDate");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getAvailableDate());
						System.out.println("/n");
					}
					if (reportInfoList.isSetAcknowledged()) {
						System.out.println("                Acknowledged");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.isAcknowledged());
						System.out.println("/n");
					}
					if (reportInfoList.isSetAcknowledgedDate()) {
						System.out.println("                AcknowledgedDate");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getAcknowledgedDate());
						System.out.println("/n");
					}
				}
			}
			if (response.isSetResponseMetadata()) {
				System.out.println("        ResponseMetadata");
				System.out.println("/n");
				ResponseMetadata responseMetadata = response.getResponseMetadata();
				if (responseMetadata.isSetRequestId()) {
					System.out.println("            RequestId");
					System.out.println("/n");
					System.out.println("                " + responseMetadata.getRequestId());
					System.out.println("/n");
				}
			}
			System.out.println("/n");
			System.out.println(response.getResponseHeaderMetadata().toString());
			System.out.println("/n");

			return response;

		} catch (MarketplaceWebServiceException ex) {

			System.out.println("Caught Exception: " + ex.getMessage());
			System.out.println("Response Status Code: " + ex.getStatusCode());
			System.out.println("Error Code: " + ex.getErrorCode());
			System.out.println("Error Type: " + ex.getErrorType());
			System.out.println("Request ID: " + ex.getRequestId());
			System.out.println("XML: " + ex.getXML());
			System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
		}

		return null;
	}
}
