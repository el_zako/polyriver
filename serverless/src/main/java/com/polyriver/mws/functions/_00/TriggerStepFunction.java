package com.polyriver.mws.functions._00;

import java.io.IOException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.util.IOUtils;

/**
 * On S3 bucket, an event is configured to trigger this lambda to start
 * execution of step function on object put.
 * 
 * @author zarkasih.adlan
 *
 */
public class TriggerStepFunction implements RequestHandler<S3Event, String> {

	@Override
	public String handleRequest(S3Event event, Context context) {

		// load S3 event which triggered this lambda (one put event triggers one lambda)
		S3EventNotificationRecord record = event.getRecords().get(0);

		context.getLogger().log("Lambda triggered by S3 object: " + record.getS3().getObject().getKey());

		String bucket = record.getS3().getBucket().getName();
		String key = record.getS3().getObject().getUrlDecodedKey();

		context.getLogger().log("bucket: " + bucket);
		context.getLogger().log("key: " + key);

		AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
		S3Object s3Object = s3Client.getObject(new GetObjectRequest(bucket, key));

		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
		String outputText = null;
		try {
			outputText = IOUtils.toString(objectInputStream);
			context.getLogger().log("Object contents: " + outputText);
		} catch (IOException e) {
			context.getLogger().log("Error in reading object from S3ObjectInputStream");
		}

		// execute step function based on ARN
		StartExecutionRequest ser = new StartExecutionRequest();
		ser.setStateMachineArn("arn:aws:states:ap-southeast-1:123682140254:stateMachine:GetOrderDataFromMWS");

		final AWSStepFunctions client = AWSStepFunctionsClientBuilder.defaultClient();
		ser.withInput(outputText);
		client.startExecution(ser);

		context.getLogger().log(ser.toString() + " executed.");

		return context.toString();
	}

}
