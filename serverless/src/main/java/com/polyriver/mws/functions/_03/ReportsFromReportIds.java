package com.polyriver.mws.functions._03;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.GetReportRequest;
import com.amazonaws.mws.model.GetReportResponse;
import com.amazonaws.mws.model.ResponseMetadata;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * With list of report ids from previous steps, get the report from MWS API.
 * 
 * @author zarkasih.adlan
 *
 */
@SuppressWarnings("rawtypes")
public class ReportsFromReportIds implements RequestHandler<Map, Object> {

	static final String appName = "Polyriver";
	static final String appVersion = "0.1";

	static String bucketName = "merchant-mws-report-dev";

	@SuppressWarnings("unchecked")
	@Override
	public Object handleRequest(Map input, Context context) {
		MarketplaceWebService service = initMarketplaceWebService(input, context.getLogger());

		String id = input.get("id").toString();
		String merchantId = input.get("account_id").toString();
		List<String> reportIds = (List<String>) input.get("reportIds");

		AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

		for (String reportId : reportIds) {
			context.getLogger().log("Getting report with id: " + reportId);
			String report = getReport(merchantId, service, reportId);

			s3Client.putObject(bucketName, id + "/" + reportId, report);
		}

		return context;
	}

	private String getReport(String merchantId, MarketplaceWebService service, String reportId) {

		GetReportRequest request = new GetReportRequest();
		request.setMerchant(merchantId);
		// request.setMWSAuthToken(sellerDevAuthToken);

		request.setReportId(reportId);

		// Note that depending on the type of report being downloaded, a report can
		// reach
		// sizes greater than 1GB. For this reason we recommend that you _always_
		// program to
		// MWS in a streaming fashion. Otherwise, as your business grows you may
		// silently reach
		// the in-memory size limit and have to re-work your solution.
		//

		ByteArrayOutputStream report = new ByteArrayOutputStream();
		request.setReportOutputStream(report);

		GetReportResponse reportResponse = invokeGetReport(service, request);

		System.out.println(reportResponse.toXML());

		return request.getReportOutputStream().toString();

	}

	/**
	 * Get Report request sample The GetReport operation returns the contents of a
	 * report. Reports can potentially be very large (>100MB) which is why we only
	 * return one report at a time, and in a streaming fashion.
	 * 
	 * @param service
	 *            instance of MarketplaceWebService service
	 * @param request
	 *            Action to invoke
	 */
	private GetReportResponse invokeGetReport(MarketplaceWebService service, GetReportRequest request) {
		GetReportResponse response = null;

		try {

			response = service.getReport(request);

			System.out.println("GetReport Action Response");
			System.out.println("=============================================================================");
			System.out.println("/n");

			System.out.println("    GetReportResponse");
			System.out.println("/n");
			System.out.println("    GetReportResult");
			System.out.println("/n");
			System.out.println("            MD5Checksum");
			System.out.println("/n");
			System.out.println("                " + response.getGetReportResult().getMD5Checksum());
			System.out.println("/n");
			if (response.isSetResponseMetadata()) {
				System.out.println("        ResponseMetadata");
				System.out.println("/n");
				ResponseMetadata responseMetadata = response.getResponseMetadata();
				if (responseMetadata.isSetRequestId()) {
					System.out.println("            RequestId");
					System.out.println("/n");
					System.out.println("                " + responseMetadata.getRequestId());
					System.out.println("/n");
				}
			}
			System.out.println("/n");

			System.out.println("Report");
			System.out.println("=============================================================================");
			System.out.println("/n");
			System.out.println(request.getReportOutputStream().toString());
			System.out.println("/n");

			System.out.println(response.getResponseHeaderMetadata().toString());
			System.out.println("/n");

		} catch (MarketplaceWebServiceException ex) {

			System.out.println("Caught Exception: " + ex.getMessage());
			System.out.println("Response Status Code: " + ex.getStatusCode());
			System.out.println("Error Code: " + ex.getErrorCode());
			System.out.println("Error Type: " + ex.getErrorType());
			System.out.println("Request ID: " + ex.getRequestId());
			System.out.println("XML: " + ex.getXML());
			System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
		}
		return response;
	}

	private MarketplaceWebService initMarketplaceWebService(Map input, LambdaLogger logger) {
		String accessKey = input.get("access_key").toString();
		String secretKey = input.get("secret_key").toString();
		String endpoint = input.get("endpoint").toString();

		logger.log("accessKey " + accessKey);
		logger.log("secretKey " + secretKey);
		logger.log("endpoint " + endpoint);

		MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
		config.setServiceURL(endpoint);
		MarketplaceWebServiceClient service = new MarketplaceWebServiceClient(accessKey, secretKey, appName, appVersion,
				config);

		logger.log("MarketplaceWebServiceClient initialized: " + service.toString());

		return service;
	}

}
