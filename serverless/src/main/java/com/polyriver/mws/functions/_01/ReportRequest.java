package com.polyriver.mws.functions._01;

import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.ReportRequestInfo;
import com.amazonaws.mws.model.RequestReportRequest;
import com.amazonaws.mws.model.RequestReportResponse;
import com.amazonaws.mws.model.RequestReportResult;
import com.amazonaws.mws.model.ResponseMetadata;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * Request for report and gets report request ID from MWS API.
 * 
 * @author zarkasih.adlan
 *
 */
@SuppressWarnings("rawtypes")
public class ReportRequest implements RequestHandler<Map, String> {

	static final String appName = "Polyriver";
	static final String appVersion = "0.1";

	@Override
	public String handleRequest(Map input, Context context) {

		MarketplaceWebService service = initMarketplaceWebService(input, context.getLogger());

		DatatypeFactory df = null;
		try {
			df = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			context.getLogger().log("Error getting new instance of DatatypeFactory " + e.getMessage());
		}

		LocalDate date30DaysAgo = LocalDate.now().minusMonths(2);
		LocalDate dateYesterday = LocalDate.now().minusDays(1);
		XMLGregorianCalendar startDate = getStartDate(date30DaysAgo, df);
		XMLGregorianCalendar endDate = getEndDate(dateYesterday, df);

		String requestReportId = requestReport(input.get("account_id").toString(), service, startDate, endDate,
				context.getLogger());

		return requestReportId;
	}

	private XMLGregorianCalendar getStartDate(LocalDate date, DatatypeFactory df) {
		GregorianCalendar firstMomentOfYesterday = new GregorianCalendar(date.getYear(),
				date.getMonthValue(), date.getDayOfMonth(), 0, 0, 0);
		XMLGregorianCalendar firstMomentOfYesterdayAsXMLGregorianCalendar = df
				.newXMLGregorianCalendar(firstMomentOfYesterday);
		firstMomentOfYesterdayAsXMLGregorianCalendar.setTimezone(0);

		return firstMomentOfYesterdayAsXMLGregorianCalendar;
	}

	private XMLGregorianCalendar getEndDate(LocalDate dateYesterday, DatatypeFactory df) {
		GregorianCalendar lastMomentOfYesterday = new GregorianCalendar(dateYesterday.getYear(),
				dateYesterday.getMonthValue(), dateYesterday.getDayOfMonth(), 23, 59, 59);
		XMLGregorianCalendar lastMomentOfYesterdayAsXMLGregorianCalendar = df
				.newXMLGregorianCalendar(lastMomentOfYesterday);
		lastMomentOfYesterdayAsXMLGregorianCalendar.setTimezone(0);
		lastMomentOfYesterdayAsXMLGregorianCalendar.setMillisecond(999);

		return lastMomentOfYesterdayAsXMLGregorianCalendar;
	}

	private MarketplaceWebService initMarketplaceWebService(Map input, LambdaLogger logger) {
		String accessKey = input.get("access_key").toString();
		String secretKey = input.get("secret_key").toString();
		String endpoint = input.get("endpoint").toString();

		logger.log("accessKey " + accessKey);
		logger.log("secretKey " + secretKey);
		logger.log("endpoint " + endpoint);

		MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
		config.setServiceURL(endpoint);
		MarketplaceWebServiceClient service = new MarketplaceWebServiceClient(accessKey, secretKey, appName, appVersion,
				config);

		logger.log("MarketplaceWebServiceClient initialized: " + service.toString());

		return service;
	}

	private String requestReport(String merchantId, MarketplaceWebService service, XMLGregorianCalendar startDate,
			XMLGregorianCalendar endDate, LambdaLogger logger) {

		String reportRequestId = null;

		RequestReportRequest request = new RequestReportRequest().withMerchant(merchantId)
				.withReportType("_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_")
				.withReportOptions("ShowSalesChannel=true").withStartDate(startDate).withEndDate(endDate);

		RequestReportResponse requestReportResponse = invokeRequestReport(service, request);
		if (requestReportResponse != null) {
			reportRequestId = requestReportResponse.getRequestReportResult().getReportRequestInfo()
					.getReportRequestId();

			logger.log((requestReportResponse.toXML()));
		}

		return reportRequestId;
	}

	/**
	 * Request Report request sample requests the generation of a report
	 * 
	 * @param service
	 *            instance of MarketplaceWebService service
	 * @param request
	 *            Action to invoke
	 */
	public RequestReportResponse invokeRequestReport(MarketplaceWebService service, RequestReportRequest request) {
		try {

			RequestReportResponse response = service.requestReport(request);

			System.out.println("RequestReport Action Response");
			System.out.println("=============================================================================");
			System.out.println("/n");

			System.out.println("    RequestReportResponse");
			System.out.println("/n");
			if (response.isSetRequestReportResult()) {
				System.out.println("        RequestReportResult");
				System.out.println("/n");
				RequestReportResult requestReportResult = response.getRequestReportResult();
				if (requestReportResult.isSetReportRequestInfo()) {
					System.out.println("            ReportRequestInfo");
					System.out.println("/n");
					ReportRequestInfo reportRequestInfo = requestReportResult.getReportRequestInfo();
					if (reportRequestInfo.isSetReportRequestId()) {
						System.out.println("                ReportRequestId");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getReportRequestId());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetReportType()) {
						System.out.println("                ReportType");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getReportType());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetStartDate()) {
						System.out.println("                StartDate");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getStartDate());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetEndDate()) {
						System.out.println("                EndDate");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getEndDate());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetSubmittedDate()) {
						System.out.println("                SubmittedDate");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getSubmittedDate());
						System.out.println("/n");
					}
					if (reportRequestInfo.isSetReportProcessingStatus()) {
						System.out.println("                ReportProcessingStatus");
						System.out.println("/n");
						System.out.println("                    " + reportRequestInfo.getReportProcessingStatus());
						System.out.println("/n");
					}
				}
			}
			if (response.isSetResponseMetadata()) {
				System.out.println("        ResponseMetadata");
				System.out.println("/n");
				ResponseMetadata responseMetadata = response.getResponseMetadata();
				if (responseMetadata.isSetRequestId()) {
					System.out.println("            RequestId");
					System.out.println("/n");
					System.out.println("                " + responseMetadata.getRequestId());
					System.out.println("/n");
				}
			}
			System.out.println("/n");
			System.out.println(response.getResponseHeaderMetadata().toString());
			System.out.println("/n");

			return response;

		} catch (MarketplaceWebServiceException ex) {

			System.out.println("Caught Exception: " + ex.getMessage());
			System.out.println("Response Status Code: " + ex.getStatusCode());
			System.out.println("Error Code: " + ex.getErrorCode());
			System.out.println("Error Type: " + ex.getErrorType());
			System.out.println("Request ID: " + ex.getRequestId());
			System.out.println("XML: " + ex.getXML());
			System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
		}

		return null;
	}

}
