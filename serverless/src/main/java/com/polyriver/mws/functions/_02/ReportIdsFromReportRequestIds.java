package com.polyriver.mws.functions._02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.GetReportListRequest;
import com.amazonaws.mws.model.GetReportListResponse;
import com.amazonaws.mws.model.GetReportListResult;
import com.amazonaws.mws.model.IdList;
import com.amazonaws.mws.model.ReportInfo;
import com.amazonaws.mws.model.ResponseMetadata;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * Request for report list using report request id from previous step. Gets list
 * of report ids from MWS API.
 * 
 * @author zarkasih.adlan
 *
 */
@SuppressWarnings("rawtypes")
public class ReportIdsFromReportRequestIds implements RequestHandler<Map, List<String>> {

	static final String appName = "Polyriver";
	static final String appVersion = "0.1";

	@Override
	public List<String> handleRequest(Map input, Context context) {

		MarketplaceWebService service = initMarketplaceWebService(input, context.getLogger());

		// requestId is populated in previous step in state machine
		String requestId = input.get("requestId").toString();
		String merchantId = input.get("account_id").toString();

		context.getLogger().log("Merchant ID: " + merchantId);
		context.getLogger().log("Request ID: " + requestId);

		IdList reportRequestIdList = new IdList();
		reportRequestIdList.setId(Collections.singletonList(requestId));

		List<String> reportIds = getReportList(merchantId, service, reportRequestIdList, context);

		return reportIds;
	}

	private MarketplaceWebService initMarketplaceWebService(Map input, LambdaLogger logger) {
		String accessKey = input.get("access_key").toString();
		String secretKey = input.get("secret_key").toString();
		String endpoint = input.get("endpoint").toString();

		logger.log("accessKey " + accessKey);
		logger.log("secretKey " + secretKey);
		logger.log("endpoint " + endpoint);

		MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
		config.setServiceURL(endpoint);
		MarketplaceWebServiceClient service = new MarketplaceWebServiceClient(accessKey, secretKey, appName, appVersion,
				config);

		logger.log("MarketplaceWebServiceClient initialized: " + service.toString());

		return service;
	}

	private List<String> getReportList(String merchantId, MarketplaceWebService service, IdList reportRequestIdList,
			Context context) {
		GetReportListRequest request = new GetReportListRequest();
		request.setMerchant(merchantId);

		// request.setReportTypeList(new
		// TypeList(Arrays.asList("_GET_MERCHANT_LISTINGS_ALL_DATA_")));
		// request.withReportTypeList(new
		// TypeList(Arrays.asList("_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_")));
		request.setReportRequestIdList(reportRequestIdList);

		GetReportListResponse response = invokeGetReportList(service, request);
		System.out.println(response.toXML());

		List<ReportInfo> infoList = response.getGetReportListResult().getReportInfoList();

		List<String> reportIdList = new ArrayList<>();
		infoList.stream().map(ReportInfo::getReportId).forEach(reportIdList::add);

		System.out.println("reportIdList " + reportIdList.toString());

		return reportIdList;
	}

	/**
	 * Get Report List request sample returns a list of reports; by default the most
	 * recent ten reports, regardless of their acknowledgement status
	 * 
	 * @param service
	 *            instance of MarketplaceWebService service
	 * @param request
	 *            Action to invoke
	 */
	public GetReportListResponse invokeGetReportList(MarketplaceWebService service, GetReportListRequest request) {
		try {

			GetReportListResponse response = service.getReportList(request);

			System.out.println("GetReportList Action Response");
			System.out.println("=============================================================================");
			System.out.println("/n");

			System.out.println("    GetReportListResponse");
			System.out.println("/n");
			if (response.isSetGetReportListResult()) {
				System.out.println("        GetReportListResult");
				System.out.println("/n");
				GetReportListResult getReportListResult = response.getGetReportListResult();
				if (getReportListResult.isSetNextToken()) {
					System.out.println("            NextToken");
					System.out.println("/n");
					System.out.println("                " + getReportListResult.getNextToken());
					System.out.println("/n");
				}
				if (getReportListResult.isSetHasNext()) {
					System.out.println("            HasNext");
					System.out.println("/n");
					System.out.println("                " + getReportListResult.isHasNext());
					System.out.println("/n");
				}
				java.util.List<ReportInfo> reportInfoListList = getReportListResult.getReportInfoList();
				for (ReportInfo reportInfoList : reportInfoListList) {
					System.out.println("            ReportInfoList");
					System.out.println("/n");
					if (reportInfoList.isSetReportId()) {
						System.out.println("                ReportId");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getReportId());
						System.out.println("/n");
					}
					if (reportInfoList.isSetReportType()) {
						System.out.println("                ReportType");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getReportType());
						System.out.println("/n");
					}
					if (reportInfoList.isSetReportRequestId()) {
						System.out.println("                ReportRequestId");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getReportRequestId());
						System.out.println("/n");
					}
					if (reportInfoList.isSetAvailableDate()) {
						System.out.println("                AvailableDate");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getAvailableDate());
						System.out.println("/n");
					}
					if (reportInfoList.isSetAcknowledged()) {
						System.out.println("                Acknowledged");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.isAcknowledged());
						System.out.println("/n");
					}
					if (reportInfoList.isSetAcknowledgedDate()) {
						System.out.println("                AcknowledgedDate");
						System.out.println("/n");
						System.out.println("                    " + reportInfoList.getAcknowledgedDate());
						System.out.println("/n");
					}
				}
			}
			if (response.isSetResponseMetadata()) {
				System.out.println("        ResponseMetadata");
				System.out.println("/n");
				ResponseMetadata responseMetadata = response.getResponseMetadata();
				if (responseMetadata.isSetRequestId()) {
					System.out.println("            RequestId");
					System.out.println("/n");
					System.out.println("                " + responseMetadata.getRequestId());
					System.out.println("/n");
				}
			}
			System.out.println("/n");
			System.out.println(response.getResponseHeaderMetadata().toString());
			System.out.println("/n");

			return response;

		} catch (MarketplaceWebServiceException ex) {

			System.out.println("Caught Exception: " + ex.getMessage());
			System.out.println("Response Status Code: " + ex.getStatusCode());
			System.out.println("Error Code: " + ex.getErrorCode());
			System.out.println("Error Type: " + ex.getErrorType());
			System.out.println("Request ID: " + ex.getRequestId());
			System.out.println("XML: " + ex.getXML());
			System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
		}

		return null;
	}

}
