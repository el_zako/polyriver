package com.polyriver.mws.preprocessing;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.postgresql.ds.PGSimpleDataSource;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.polyriver.app.model.Merchant;

/**
 * This function queries the merchant database and puts the merchant data in S3.
 * This is trigerred by CloudWatch Events on a fixed interval.
 * 
 * @author zarkasih.adlan
 *
 */
@SuppressWarnings("rawtypes")
public class PutMerchantDataInS3 implements RequestHandler<Map, Object> {

	static String bucketName = "merchant-metadata-dev";

	@Override
	public Object handleRequest(Map input, Context context) {

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HHmmss");
		String formattedDateTime = now.format(formatter);
		context.getLogger().log("Timestamp: " + formattedDateTime);

		List<Merchant> merchants = findAllMerchants(context);
		AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

		ObjectMapper mapper = new ObjectMapper();

		for (Merchant merchantDetails : merchants) {
			context.getLogger().log("Processing merchant: " + merchantDetails.toString());

			String key = merchantDetails.getId() + "/" + merchantDetails.getMarketplace_name() + "/"
					+ formattedDateTime;

			String jsonValue = "";
			try {
				jsonValue = mapper.writeValueAsString(merchantDetails);
			} catch (JsonProcessingException e1) {
				context.getLogger().log("Error converting object to JSON");
			}

			s3Client.putObject(bucketName, key, jsonValue);

		}

		return context;
	}

	private List<Merchant> findAllMerchants(Context context) {
		List<Merchant> merchants = null;
		try {

			PGSimpleDataSource dataSource = new PGSimpleDataSource();
			// dataSource.setServerName("localhost");
			// dataSource.setDatabaseName("postgres");
			// dataSource.setCurrentSchema("public");
			// dataSource.setUser("pdev");
			// dataSource.setPassword("polyriver");
			// dataSource.setPortNumber(5432);

			dataSource.setServerName("prdev01.cuycdqeuverg.ap-southeast-1.rds.amazonaws.com");
			dataSource.setDatabaseName("polyriver_db_001");
			dataSource.setCurrentSchema("public");
			dataSource.setUser("polyriver_dev");
			dataSource.setPassword("polyriverdev2018");
			dataSource.setPortNumber(5432);

			QueryRunner runner = new QueryRunner(dataSource);

			ResultSetHandler<List<Merchant>> merchantHandler = new BeanListHandler<Merchant>(Merchant.class);

			StringBuilder query = new StringBuilder();
			query.append(
					"SELECT m.id,m.email,mk.account_id,mk.access_key,mk.secret_key,mk.endpoint,mk.marketplace_name ")
					.append("FROM merchant m ").append("LEFT JOIN merchant_key mk ").append("ON m.id=mk.merchant_id ")
					.append("WHERE m.enabled='true' AND mk.enabled='true'");

			merchants = runner.query(query.toString(), merchantHandler);

		} catch (SQLException e) {
			context.getLogger().log("Error finding merchants from database: " + e.getMessage());
		}
		return merchants;
	}

}
