package com.polyriver.app.model;

public class Merchant {

	private Long id;

	private String email;

	private String account_id;

	private String access_key;

	private String secret_key;

	private String endpoint;

	private String marketplace_name;

	public Merchant() {
		// Empty constructor is needed by Spring Data / JPA
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public String getAccess_key() {
		return access_key;
	}

	public void setAccess_key(String access_key) {
		this.access_key = access_key;
	}

	public String getSecret_key() {
		return secret_key;
	}

	public void setSecret_key(String secret_key) {
		this.secret_key = secret_key;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getMarketplace_name() {
		return marketplace_name;
	}

	public void setMarketplace_name(String marketplace_name) {
		this.marketplace_name = marketplace_name;
	}

	@Override
	public String toString() {
		return "Merchant [id=" + id + ", email=" + email + ", account_id=" + account_id + ", access_key=" + access_key
				+ ", secret_key=" + secret_key + ", endpoint=" + endpoint + ", marketplace_name=" + marketplace_name
				+ "]";
	}

	//TODO
	public String toJSON() {
		return "{id=" + id + ", email=" + email + ", account_id=" + account_id + ", access_key=" + access_key
				+ ", secret_key=" + secret_key + ", endpoint=" + endpoint + ", marketplace_name=" + marketplace_name
				+ "}";
	}

}
