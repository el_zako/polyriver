# Polyriver

This project aggregates business data of online sellers from multiple marketplaces like Amazon, Ebay, Shopify into one view.

## Getting Started

### Project Structure

#### data

Data access layer using Spring Data JPA

#### seller central

View layer using Vaadin 8 with Dashboard and Charts

#### serverless

Data aggregation layer using serverless architecture.


## Built With
* [Spring Boot](https://spring.io/projects/spring-boot) - Bootstraped Spring application
* [Vaadin](https://vaadin.com/) - Web framework 
* [AWS](https://aws.amazon.com/) - Cloud computing services
	* [Lambda](https://aws.amazon.com/lambda/) - Serverless compute
	* [Step Functions](https://aws.amazon.com/step-functions/) - Data processing & application orchestration
	* [S3](https://aws.amazon.com/s3/) - Cloud object storage
	* [RDS](https://aws.amazon.com/rds/) - Cloud relational database
* [Maven](https://maven.apache.org/) - Dependency management



